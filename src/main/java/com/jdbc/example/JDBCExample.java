package com.jdbc.example;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCExample {

	public static void main(String[] args) {
		Properties prop = new Properties();
		InputStream input = null;

		input = ClassLoader.getSystemResourceAsStream("jdbc.properties");
		Connection conn = null;
		//load DB connection info
		try {
			prop.load(input);
			String driverClassName = prop.getProperty("jdbc.driverClassName");
			String url = prop.getProperty("jdbc.url");
			String userName = prop.getProperty("jdbc.username");
			String password = prop.getProperty("jdbc.password");

			// load JDBC driver
			Class.forName(driverClassName).newInstance();

			// connect to mysql database
			conn = DriverManager.getConnection(url, userName, password);
			//query to get orders placed by each customer
			String query = "SELECT orders.order_id, orders.order_date, orders.amount, customers.name "
					+ "FROM customers, orders WHERE customers.customer_id=orders.customer_id ORDER BY customers.name";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs != null) {
				System.out.println("Orders------Customers------");
				System.out.println("ID   Order Date   Amount    Customer");
				while (rs.next()) {
					System.out.println(
							String.format(
									"%d    %s   %.2f    %s",
									rs.getInt("order_id"),
									rs.getDate("order_date"),
									rs.getDouble("amount"),
									rs.getString("name")));
				}
			}
			else {
				System.out.println("Empty data source.");
			}
			rs.close();
			stmt.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (InstantiationException e) {
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if (conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
